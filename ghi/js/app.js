function createCard(name, description, pictureUrl, start, end, location) {
    return `
      <div class="card mb-3 shadow">

      <img src="${pictureUrl}" class="card-img-top">


        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          ${start}-${end}
       </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const start = new Date(details.conference.starts).toLocaleDateString();

            console.log(start)
            const end = new Date(details.conference.ends).toLocaleDateString();
            const location = details.conference.location.name;
            console.log(location)
            const html = createCard(name, description, pictureUrl, start, end, location);
            const column = document.querySelector('.col');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }

  });
